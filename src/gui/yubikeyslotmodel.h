// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include "osutils/DeviceListener.h"
#include <QAbstractListModel>
#include <qqmlregistration.h>

class YubikeySlotModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

    /// This property holds whether the hardware keys are currently getting ṕolled.
    Q_PROPERTY(bool pollingHardwareKey READ pollingHardwareKey WRITE setPollingHardwareKey NOTIFY pollingHardwareKeyChanged)

public:
    enum ExtraRole {
        ValueRole = Qt::UserRole + 1,
    };

    explicit YubikeySlotModel(QObject *parent = nullptr);
    ~YubikeySlotModel() override;

    [[nodiscard]] int rowCount(const QModelIndex &parent = {}) const override;
    [[nodiscard]] QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    [[nodiscard]] QHash<int, QByteArray> roleNames() const override;

    [[nodiscard]] bool pollingHardwareKey() const;
    void setPollingHardwareKey(bool pollingHardwareKey);

    void setFileName(const QString &filename);

public Q_SLOTS:
    void pollHardwareKey(bool manualTrigger = false);
    void hardwareKeyResponse(bool found);

Q_SIGNALS:
    void pollingHardwareKeyChanged();
    void toggleHardwareKeyComponent(bool state);
    void lastSelectedIndexFound(int index);

private:
    void refresh(bool found);
    bool m_pollingHardwareKey = false;
    QPointer<DeviceListener> m_deviceListener;
    QString m_filename;
    bool m_useHardwareKey = false;
    bool m_manualHardwareKeyRefresh = false;
};
