// SPDX-FileCopyrightText: 2011 Felix Geyer <debfx@fobos.de>
// SPDX-FileCopyrightText: 2022 KeePassXC Team <team@keepassxc.org>
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#pragma once

#include "core/Group.h"
#include <QObject>
#include <qqmlregistration.h>

class GroupEditorBackend : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    /// This property holds a temporary group which holds the modifications.
    Q_PROPERTY(Group *group READ group NOTIFY groupChanged)

    /// This property holds whether the group was changed.
    Q_PROPERTY(bool modified READ isModified WRITE setModified NOTIFY modifiedChanged)

    /// This property holds whether the group is a new group.
    Q_PROPERTY(bool newGroup READ isNewGroup NOTIFY newGroupChanged)

public:
    explicit GroupEditorBackend(QObject *parent = nullptr);

    Q_INVOKABLE void loadGroup(Group *group, Group *parentGroup);

    Group *group() const;

    [[nodiscard]] bool isNewGroup() const;

    [[nodiscard]] bool isModified() const;
    void setModified(bool modified);

public Q_SLOTS:
    void apply();
    void save();
    void cancel();
    void clear();

Q_SIGNALS:
    void editFinished(bool success);
    void modifiedChanged();
    void groupChanged();
    void newGroupChanged();

private:
    std::unique_ptr<Group> m_temporaryGroup;
    QPointer<Group> m_group;
    Group *m_parentGroup = nullptr;
    Database *m_db = nullptr;
    bool m_modified = false;
    bool m_newGroup = false;
};
