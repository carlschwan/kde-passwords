/*
 *  Copyright (C) 2017 KeePassXC Team <team@keepassxc.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 or (at your option)
 *  version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScreenLockListenerDBus.h"

#include <QDBusInterface>
#include <QDebug>
#include <QProcessEnvironment>

using namespace Qt::StringLiterals;

ScreenLockListenerDBus::ScreenLockListenerDBus(QWidget *parent)
    : ScreenLockListenerPrivate(parent)
{
    QDBusConnection sessionBus = QDBusConnection::sessionBus();
    QDBusConnection systemBus = QDBusConnection::systemBus();

    sessionBus.connect(u"org.freedesktop.ScreenSaver"_s, // service
                       u"/org/freedesktop/ScreenSaver"_s, // path
                       u"org.freedesktop.ScreenSaver"_s, // interface
                       u"ActiveChanged"_s, // signal name
                       this, // receiver
                       SLOT(freedesktopScreenSaver(bool)));

    sessionBus.connect(u"org.gnome.ScreenSaver"_s, // service
                       u"/org/gnome/ScreenSaver"_s, // path
                       u"org.gnome.ScreenSaver"_s, // interface
                       u"ActiveChanged"_s, // signal name
                       this, // receiver
                       SLOT(freedesktopScreenSaver(bool)));

    sessionBus.connect(u"org.gnome.SessionManager"_s, // service
                       u"/org/gnome/SessionManager/Presence"_s, // path
                       u"org.gnome.SessionManager.Presence"_s, // interface
                       u"StatusChanged"_s, // signal name
                       this, // receiver
                       SLOT(gnomeSessionStatusChanged(uint)));

    sessionBus.connect(u"org.xfce.ScreenSaver"_s, // service
                       u"/org/xfce/ScreenSaver"_s, // path
                       u"org.xfce.ScreenSaver"_s, // interface
                       u"ActiveChanged"_s, // signal name
                       this, // receiver
                       SLOT(freedesktopScreenSaver(bool)));

    systemBus.connect(u"org.freedesktop.login1"_s, // service
                      u"/org/freedesktop/login1"_s, // path
                      u"org.freedesktop.login1.Manager"_s, // interface
                      u"PrepareForSleep"_s, // signal name
                      this, // receiver
                      SLOT(logindPrepareForSleep(bool)));

    QString sessionId = QProcessEnvironment::systemEnvironment().value(u"XDG_SESSION_ID"_s);
    QDBusInterface loginManager(u"org.freedesktop.login1"_s, // service
                                u"/org/freedesktop/login1"_s, // path
                                u"org.freedesktop.login1.Manager"_s, // interface
                                systemBus);
    if (loginManager.isValid()) {
        QList<QVariant> args = {sessionId};
        loginManager.callWithCallback(u"GetSession"_s, args, this, SLOT(login1SessionObjectReceived(QDBusMessage)));
    }

    sessionBus.connect(u"com.canonical.Unity"_s, // service
                       u"/com/canonical/Unity/Session"_s, // path
                       u"com.canonical.Unity.Session"_s, // interface
                       u"Locked"_s, // signal name
                       this, // receiver
                       SLOT(unityLocked()));
}

void ScreenLockListenerDBus::login1SessionObjectReceived(QDBusMessage response)
{
    if (response.arguments().isEmpty()) {
        qDebug() << "org.freedesktop.login1.Manager.GetSession did not return results";
        return;
    }
    QVariant arg0 = response.arguments().at(0);
    if (!arg0.canConvert<QDBusObjectPath>()) {
        qDebug() << "org.freedesktop.login1.Manager.GetSession did not return a QDBusObjectPath";
        return;
    }
    auto path = arg0.value<QDBusObjectPath>();
    QDBusConnection systemBus = QDBusConnection::systemBus();

    systemBus.connect(QString{}, // service
                      path.path(), // path
                      u"org.freedesktop.login1.Session"_s, // interface
                      u"Lock"_s, // signal name
                      this, // receiver
                      SLOT(unityLocked()));
}

void ScreenLockListenerDBus::gnomeSessionStatusChanged(uint status)
{
    if (status != 0) {
        Q_EMIT screenLocked();
    }
}

void ScreenLockListenerDBus::logindPrepareForSleep(bool beforeSleep)
{
    if (beforeSleep) {
        Q_EMIT screenLocked();
    }
}

void ScreenLockListenerDBus::unityLocked()
{
    Q_EMIT screenLocked();
}

void ScreenLockListenerDBus::freedesktopScreenSaver(bool status)
{
    if (status) {
        Q_EMIT screenLocked();
    }
}
