// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.delegates as Delegates
import org.kde.kitemmodels
import org.kde.keychain

Kirigami.OverlayDrawer {
    id: drawer

    required property Kirigami.ApplicationWindow window

    enabled: Controller.currentDatabase

    edge: Qt.application.layoutDirection === Qt.RightToLeft ? Qt.RightEdge : Qt.LeftEdge
    modal: !enabled || Kirigami.Settings.isMobile || Kirigami.Settings.tabletMode || (window.width < Kirigami.Units.gridUnit * 50 && !collapsed) // Only modal when not collapsed, otherwise collapsed won't show.
    z: modal ? Math.round(position * 10000000) : 100
    drawerOpen: !Kirigami.Settings.isMobile && enabled
    width: Kirigami.Units.gridUnit * 14

    Kirigami.Theme.colorSet: Kirigami.Theme.Window
    Kirigami.Theme.inherit: false

    handleClosedIcon.source: modal ? null : "sidebar-expand-left"
    handleOpenIcon.source: modal ? null : "sidebar-collapse-left"
    handleVisible: modal && enabled
    onModalChanged: drawerOpen = !modal;

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    Behavior on width {
        NumberAnimation {
            duration: Kirigami.Units.longDuration
            easing.type: Easing.InOutQuad
        }
    }

    contentItem: ColumnLayout {
        spacing: 0

        Controls.ToolBar {
            Layout.fillWidth: true
            Layout.preferredHeight: pageStack.globalToolBar.preferredHeight
            Layout.bottomMargin: Kirigami.Units.smallSpacing / 2

            leftPadding: 3
            rightPadding: 3
            topPadding: 3
            bottomPadding: 3

            visible: !Kirigami.Settings.isMobile

            contentItem: Kirigami.SearchField {}
        }

        Controls.ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            ListView {
                currentIndex: -1

                model: KDescendantsProxyModel {
                    id: foldersModel
                    model: GroupModel {
                        database: Controller.currentDatabase
                    }
                }

                delegate: Delegates.RoundedTreeDelegate {
                    id: delegate

                    required property string groupName
                    required property string iconName
                    required property bool isExpired
                    required property Group group

                    icon.name: iconName
                    highlighted: group === Controller.currentGroup
                    text: groupName

                    TapHandler {
                        acceptedButtons: Qt.RightButton
                        onTapped: {
                            groupMenu.group = delegate.group;
                            groupMenu.popup();
                        }
                    }

                    onClicked: {
                        Controller.currentGroup = group
                    }
                }

                Controls.Menu {
                    id: groupMenu

                    property Group group

                    Controls.MenuItem {
                        text: i18nc("@action:inmenu", "New Group")
                        onClicked: {
                            const component = Qt.createComponent('org.kde.keychain', "GroupMetadataPage")
                            if (component.status === Component.Error) {
                                console.error(component.errorString());
                                return;
                            }
                            drawer.Controls.ApplicationWindow.window.pageStack.pushDialogLayer(component, {
                                group: null,
                                parentGroup: groupMenu.group,
                            }, {
                                width: Kirigami.Units.gridUnit * 30
                            });
                        }
                    }

                    Controls.MenuItem {
                        text: i18nc("@action:inmenu", "Edit Group")
                        onClicked: {
                            const component = Qt.createComponent('org.kde.keychain', "GroupMetadataPage")
                            if (component.status === Component.Error) {
                                console.error(component.errorString());
                                return;
                            }
                            drawer.Controls.ApplicationWindow.window.pageStack.pushDialogLayer(component, {
                                group: groupMenu.group,
                                parentGroup: null,
                            }, {
                                width: Kirigami.Units.gridUnit * 30
                            });
                        }
                    }
                }
            }
        }
    }
}
