// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "controller.h"

#include <keys/drivers/YubiKey.h>

Controller::Controller(QObject *parent)
    : QObject(parent)
{
    connect(YubiKey::instance(), &YubiKey::userInteractionRequest, this, &Controller::showYubiKeyPopup, Qt::QueuedConnection);
    connect(YubiKey::instance(), &YubiKey::challengeCompleted, this, &Controller::hideYubiKeyPopup, Qt::QueuedConnection);
}

Controller::~Controller() = default;

Database *Controller::currentDatabase() const
{
    return m_currentDatabase.get();
}

void Controller::setCurrentDatabase(Database *database)
{
    if (m_currentDatabase.get() == database) {
        return;
    }
    m_currentDatabase.reset(database);
    Q_EMIT currentDatabaseChanged();

    setCurrentGroup(m_currentDatabase->rootGroup());
}

Group *Controller::currentGroup() const
{
    return m_currentGroup;
}

void Controller::setCurrentGroup(Group *group)
{
    if (m_currentGroup == group) {
        return;
    }
    m_currentGroup = group;
    Q_EMIT currentGroupChanged();
}
