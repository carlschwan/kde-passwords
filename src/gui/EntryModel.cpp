/*
 *  Copyright (C) 2010 Felix Geyer <debfx@fobos.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 or (at your option)
 *  version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EntryModel.h"
#include <KLocalizedString>
#include <QColor>
#include <QUrl>

#include "core/Database.h"
#include "core/Entry.h"
#include "core/Group.h"
#include "core/Metadata.h"
#include "core/PasswordHealth.h"
#ifdef Q_OS_MACOS
#include "gui/osutils/macutils/MacUtils.h"
#endif

EntryModel::EntryModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

Entry *EntryModel::entryFromIndex(const QModelIndex &index) const
{
    Q_ASSERT(index.isValid() && index.row() < m_entries.size());
    return m_entries.at(index.row());
}

QModelIndex EntryModel::indexFromEntry(Entry *entry) const
{
    const int row = m_entries.indexOf(entry);
    if (row >= 0) {
        return index(row, 1);
    }
    return {};
}

Group *EntryModel::group() const
{
    return m_group;
}

void EntryModel::setGroup(Group *group)
{
    if (!group || group == m_group) {
        return;
    }

    beginResetModel();

    severConnections();

    m_group = group;
    m_allGroups.clear();
    m_entries = group->entries();
    m_orgEntries.clear();

    makeConnections(group);

    endResetModel();

    Q_EMIT groupChanged();
}

void EntryModel::setEntries(const QList<Entry *> &entries)
{
    beginResetModel();

    severConnections();

    m_group = nullptr;
    m_allGroups.clear();
    m_entries = entries;
    m_orgEntries = entries;

    for (const auto entry : std::as_const(m_entries)) {
        if (entry->group()) {
            m_allGroups.insert(entry->group());
        }
    }

    for (const auto group : std::as_const(m_allGroups)) {
        makeConnections(group);
    }

    endResetModel();
}

int EntryModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    } else {
        return m_entries.size();
    }
}

QVariant EntryModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return {};
    }

    Entry *entry = entryFromIndex(index);
    EntryAttributes *attr = entry->attributes();

    QString result;
    switch (role) {
    case ParentGroupRole:
        if (entry->group()) {
            return entry->group()->name();
        }
        break;
    case TitleRole:
        result = entry->resolveMultiplePlaceholders(entry->title());
        if (attr->isReference(EntryAttributes::TitleKey)) {
            result.prepend(i18nc("Reference abbreviation", "Ref: "));
        }
        return result;
    case UsernameRole:
        result = entry->resolveMultiplePlaceholders(entry->username());
        if (attr->isReference(EntryAttributes::UserNameKey)) {
            result.prepend(i18nc("Reference abbreviation", "Ref: "));
        }
        if (entry->username().isEmpty()) {
            result = QString{};
        }
        return result;
    case UrlRole:
        result = entry->resolveMultiplePlaceholders(entry->displayUrl());
        if (attr->isReference(EntryAttributes::URLKey)) {
            result.prepend(i18nc("Reference abbreviation", "Ref: "));
        }
        return result;
    case DomainRole: {
        const auto url = QUrl::fromUserInput(entry->resolveMultiplePlaceholders(entry->displayUrl()));
        return url.host();
    }
    case ExpiryTimeRole:
        // There seems to be no better way of expressing 'infinity'
        return entry->timeInfo().expires() ? entry->timeInfo().expiryTime() : QDate(9999, 1, 1).startOfDay();
    case CreatedRole:
        return entry->timeInfo().creationTime().toLocalTime();
    case ModifiedRole:
        return entry->timeInfo().lastModificationTime().toLocalTime();
    case AccessedRole:
        return entry->timeInfo().lastAccessTime().toLocalTime();
    case BackgroundColorRole: {
        QColor color = QColor::fromString(entry->backgroundColor());
        if (color.isValid()) {
            return color;
        } else {
            return {};
        }
    }
    case ForegroundColorRole: {
        QColor color = QColor::fromString(entry->foregroundColor());
        if (color.isValid()) {
            return color;
        } else {
            return {};
        }
    }
    case TotpRole:
        return entry->hasTotp();
    case PasswordStrengthRole:
        if (!entry->password().isEmpty() && !entry->excludeFromReports()) {
            return QVariant::fromValue(entry->passwordHealth()->quality());
        } else {
            return {};
        }
    case EntryRole:
        return QVariant::fromValue(entry);
    }
    return {};
}

QHash<int, QByteArray> EntryModel::roleNames() const
{
    return {
        {ParentGroupRole, "parentGroup"},
        {TitleRole, "title"},
        {UsernameRole, "username"},
        {UrlRole, "url"},
        {DomainRole, "domain"},
        {ExpiryTimeRole, "expiryTime"},
        {CreatedRole, "created"},
        {ModifiedRole, "modified"},
        {AccessedRole, "accessed"},
        {TotpRole, "totp"},
        {PasswordStrengthRole, "passwordStrength"},
        {BackgroundColorRole, "backgroundColor"},
        {ForegroundColorRole, "foregroundColor"},
        {EntryRole, "entry"},
    };
}

void EntryModel::entryAboutToAdd(Entry *entry)
{
    if (!m_group && !m_orgEntries.contains(entry)) {
        return;
    }

    beginInsertRows(QModelIndex(), m_entries.size(), m_entries.size());
    if (!m_group) {
        m_entries.append(entry);
    }
}

void EntryModel::entryAdded(Entry *entry)
{
    if (!m_group && !m_orgEntries.contains(entry)) {
        return;
    }

    if (m_group) {
        m_entries = m_group->entries();
    }
    endInsertRows();
}

void EntryModel::entryAboutToRemove(Entry *entry)
{
    beginRemoveRows(QModelIndex(), m_entries.indexOf(entry), m_entries.indexOf(entry));
    if (!m_group) {
        m_entries.removeAll(entry);
    }
}

void EntryModel::entryRemoved()
{
    if (m_group) {
        m_entries = m_group->entries();
    }
    endRemoveRows();
}

void EntryModel::entryAboutToMoveUp(int row)
{
    beginMoveRows(QModelIndex(), row, row, QModelIndex(), row - 1);
    if (m_group) {
        m_entries.move(row, row - 1);
    }
}

void EntryModel::entryMovedUp()
{
    if (m_group) {
        m_entries = m_group->entries();
    }
    endMoveRows();
}

void EntryModel::entryAboutToMoveDown(int row)
{
    beginMoveRows(QModelIndex(), row, row, QModelIndex(), row + 2);
    if (m_group) {
        m_entries.move(row, row + 1);
    }
}

void EntryModel::entryMovedDown()
{
    if (m_group) {
        m_entries = m_group->entries();
    }
    endMoveRows();
}

void EntryModel::entryDataChanged(Entry *entry)
{
    int row = m_entries.indexOf(entry);
    Q_EMIT dataChanged(index(row, 0), index(row, 0));
}

void EntryModel::severConnections()
{
    if (m_group) {
        disconnect(m_group, nullptr, this, nullptr);
    }

    for (const Group *group : asConst(m_allGroups)) {
        disconnect(group, nullptr, this, nullptr);
    }
}

void EntryModel::makeConnections(const Group *group)
{
    connect(group, &Group::entryAboutToAdd, this, &EntryModel::entryAboutToAdd);
    connect(group, &Group::entryAdded, this, &EntryModel::entryAdded);
    connect(group, &Group::entryAboutToRemove, this, &EntryModel::entryAboutToRemove);
    connect(group, &Group::entryRemoved, this, &EntryModel::entryRemoved);
    connect(group, &Group::entryAboutToMoveUp, this, &EntryModel::entryAboutToMoveUp);
    connect(group, &Group::entryMovedUp, this, &EntryModel::entryMovedUp);
    connect(group, &Group::entryAboutToMoveDown, this, &EntryModel::entryAboutToMoveDown);
    connect(group, &Group::entryMovedDown, this, &EntryModel::entryMovedDown);
    connect(group, &Group::entryDataChanged, this, &EntryModel::entryDataChanged);
}

void EntryModel::setBackgroundColorVisible(bool visible)
{
    m_backgroundColorVisible = visible;
}

Entry *EntryModel::createEntry()
{
    m_newEntry.reset(new Entry());
    m_newEntry->setUuid(QUuid::createUuid());
    m_newEntry->setUsername(m_group->database()->metadata()->defaultUserName());

    m_group->applyGroupIconOnCreateTo(m_newEntry.get());
    return m_newEntry.get();
}

void EntryModel::saveNewEntry()
{
    m_newEntry->setGroup(m_group);
    Q_UNUSED(m_newEntry.release());
}

void EntryModel::deleteEntries(const QList<Entry *> &entries, bool force)
{
    const auto recycleBin = m_group->database()->metadata()->recycleBin();
    const bool permanent = (recycleBin && recycleBin->findEntryByUuid(entries.first()->uuid())) || !m_group->database()->metadata()->recycleBinEnabled();

    if (permanent && force) {
        for (auto entry : entries) {
            auto references = entry->database()->rootGroup()->referencesRecursive(entry);
            if (!references.isEmpty()) {
                // Ignore references that are part of this cohort
                for (auto e : entries) {
                    references.removeAll(e);
                }

                // Prompt the user on what to do with the reference (Overwrite, Delete, Skip)
                // TODO replace by non blocking code
                // auto result = MessageBox::question(
                //    parent,
                //    QObject::i18n("Replace references to entry?"),
                //    QObject::i18n(
                //        "Entry \"%1\" has %2 reference(s). "
                //        "Do you want to overwrite references with values, skip this entry, or delete anyway?",
                //        "",
                //        references.size())
                //        .arg(entry->resolvePlaceholder(entry->title()).toHtmlEscaped())
                //        .arg(references.size()),
                //    MessageBox::Overwrite | MessageBox::Skip | MessageBox::Delete,
                //    MessageBox::Overwrite);
                // if (result == MessageBox::Overwrite) {
                //    for (auto ref : references) {
                //        ref->replaceReferencesWithValues(entry);
                //    }
                //} else if (result == MessageBox::Skip) {
                //    continue;
                //}
            }

            delete entry;
        }
    } else if (permanent) {
        Q_EMIT askConfirmationPermanentDeletion(entries);
    } else {
        for (const auto entry : entries) {
            entry->database()->recycleEntry(entry);
        }
    }
}
