// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as Controls

import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.kitemmodels

import org.kde.keychain

FormCard.FormCardPage {
    id: root

    required property Group group
    required property Group parentGroup

    GroupEditorBackend {
        id: groupEditorBackend
    }

    Component.onCompleted: {
        groupEditorBackend.loadGroup(root.group, root.parentGroup);
    }

    title: groupEditorBackend.newGroup ? i18nc("@title:dialog", "Create New Group") : i18nc("@title:dialog", "Editing \"%1\"", group.name)

    header: MainBanner {}

    FormCard.FormCard {
        Layout.topMargin: Kirigami.Units.largeSpacing * 2

        FormCard.FormTextFieldDelegate {
            label: i18nc("@label:textbox", "Name:")
            text: groupEditorBackend.group?.name ?? ''
            onTextEdited: {
                groupEditorBackend.group.name = text;
                groupEditorBackend.modified = true;
            }
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormTextAreaDelegate {
            label: i18nc("@label:textbox", "Notes:")
            text: groupEditorBackend.group?.notes ?? ''
            onTextChanged: {
                groupEditorBackend.group.notes = text;
                groupEditorBackend.modified = true;
            }
        }
    }

    footer: Controls.ToolBar {
        contentItem: Controls.DialogButtonBox {
            standardButtons: Controls.Dialog.Save | Controls.Dialog.Cancel | (root.parentGroup ? Controls.Dialog.NoButton : Controls.Dialog.Apply)

            onRejected: {
                groupEditorBackend.cancel();
                root.closeDialog();
            }
            onApplied: {
                groupEditorBackend.apply();
            }
            onAccepted: {
                groupEditorBackend.save();
                root.closeDialog();
            }
        }
    }
}
