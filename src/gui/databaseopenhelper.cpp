/*
 *  Copyright (C) 2011 Felix Geyer <debfx@fobos.de>
 *  Copyright (C) 2017 KeePassXC Team <team@keepassxc.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 or (at your option)
 *  version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "databaseopenhelper.h"

#include "core/Database.h"
#include "keys/ChallengeResponseKey.h"
#include "keys/FileKey.h"
#include "keys/drivers/YubiKeyInterfaceUSB.h"
#include "quickunlock/QuickUnlockInterface.h"

#include "keychainconfig.h"

#include <KLocalizedString>

using namespace Qt::StringLiterals;

namespace
{
constexpr int clearFormsDelay = 30000;

bool isQuickUnlockAvailable()
{
    if (KeychainConfig::self()->quickUnlockEnabled()) {
        return getQuickUnlock()->isAvailable();
    }
    return false;
}
} // namespace

DatabaseOpenHelper::DatabaseOpenHelper(QObject *parent)
    : QObject(parent)
    , m_db(nullptr)
{
    connect(YubiKey::instance(), &YubiKey::userInteractionRequest, this, [this] {
        Q_EMIT showMessage(i18n("Please present or touch your YubiKey to continue…"), MessageType::Information);
    });
    connect(YubiKey::instance(), &YubiKey::challengeCompleted, this, &DatabaseOpenHelper::hideMessage);
}

DatabaseOpenHelper::~DatabaseOpenHelper() = default;

bool DatabaseOpenHelper::event(QEvent *event)
{
    // TODO this does nothing right now
    bool ret = QObject::event(event);
    auto type = event->type();

    if (type == QEvent::Show || type == QEvent::WindowActivate) {
        if (isOnQuickUnlockScreen() && (m_db == nullptr || !canPerformQuickUnlock())) {
            resetQuickUnlock();
        }
        toggleQuickUnlockScreen();

        if (type == QEvent::Show) {
            m_hideTimer.stop();
        }

        ret = true;
    } else if (type == QEvent::Hide || type == QEvent::WindowDeactivate) {
        // Schedule form clearing if we are hidden
        if (!m_hideTimer.isActive()) {
            m_hideTimer.start();
        }

        ret = true;
    }

    return ret;
}

bool DatabaseOpenHelper::unlockingDatabase() const
{
    return m_unlockingDatabase;
}

QUrl DatabaseOpenHelper::keyFileUrl() const
{
    return m_keyFileUrl;
}

void DatabaseOpenHelper::setKeyFileUrl(const QUrl &keyFileUrl)
{
    if (keyFileUrl == m_keyFileUrl) {
        return;
    }
    m_keyFileUrl = keyFileUrl;

    Q_EMIT keyFileUrlChanged();
}

QString DatabaseOpenHelper::password() const
{
    return m_password;
}

void DatabaseOpenHelper::setPassword(const QString &password)
{
    if (password == m_password) {
        return;
    }
    m_password = password;

    Q_EMIT passwordChanged();
}

void DatabaseOpenHelper::setDatabaseUrl(const QUrl &databaseUrl)
{
    if (databaseUrl == m_databaseUrl) {
        return;
    }

    m_databaseUrl = databaseUrl;
    Q_EMIT databaseUrlChanged();

    // Read public headers
    QString error;
    m_db = std::make_unique<Database>();
    m_db->open(m_databaseUrl.toLocalFile(), nullptr, &error);

    // if (config()->get(Config::RememberLastKeyFiles).toBool()) {
    //     auto lastKeyFiles = config()->get(Config::LastKeyFiles).toHash();
    //     if (lastKeyFiles.contains(m_filename)) {
    //         //m_ui->keyFileLineEdit->setText(lastKeyFiles[m_filename].toString());
    //     }
    // }

    toggleQuickUnlockScreen();
}

Database *DatabaseOpenHelper::database() const
{
    return m_db.get();
}

QUrl DatabaseOpenHelper::databaseUrl() const
{
    return m_databaseUrl;
}

void DatabaseOpenHelper::cancelDatabaseUnlockSlot()
{
    QString error;
    m_db = std::make_unique<Database>();
    m_db->open(m_databaseUrl.toLocalFile(), nullptr, &error);

    Q_EMIT showMessage(i18n("Database unlock canceled."), MessageType::Error);
    setUserInteractionLock(false);
}

void DatabaseOpenHelper::saveQuickUnlockCredentialSlot(bool blockQuickUnlock, const QSharedPointer<CompositeKey> &databaseKey)
{
    // Save Quick Unlock credentials if available
    if (!blockQuickUnlock && isQuickUnlockAvailable()) {
        const auto keyData = databaseKey->serialize();
        getQuickUnlock()->setKey(m_db->publicUuid(), keyData);
        Q_EMIT hideMessage();
    }
}

void DatabaseOpenHelper::openDatabase()
{
    // Cache this variable for future use then reset
    bool blockQuickUnlock = m_blockQuickUnlock || isOnQuickUnlockScreen();
    m_blockQuickUnlock = false;

    setUserInteractionLock(true);

    const auto databaseKey = buildDatabaseKey();
    if (!databaseKey) {
        setUserInteractionLock(false);
        return;
    }

    QString error;
    m_db = std::make_unique<Database>();
    bool ok = m_db->open(m_databaseUrl.toLocalFile(), databaseKey, &error);

    if (ok) {
        // Warn user about minor version mismatch to halt loading if necessary
        if (m_db->hasMinorVersionMismatch()) {
            Q_EMIT hasMinorVersionMismatch(blockQuickUnlock, databaseKey);
        } else {
            saveQuickUnlockCredentialSlot(blockQuickUnlock, databaseKey);
        }

        Q_EMIT finished(m_db.release()); // owernship transfered to Controller via QML
        return;
    }

    setUserInteractionLock(false);
    if (!isOnQuickUnlockScreen() && m_password.isEmpty() && !m_retryUnlockWithEmptyPassword) {
        Q_EMIT unlockFailedNoPassword();
        return;
    }

    Q_EMIT showMessage(error, DatabaseOpenHelper::Error);
}

void DatabaseOpenHelper::retryUnlockWithEmptyPassword()
{
    m_retryUnlockWithEmptyPassword = true;
    openDatabase();
}

QSharedPointer<CompositeKey> DatabaseOpenHelper::buildDatabaseKey()
{
    auto databaseKey = QSharedPointer<CompositeKey>::create();

    if (m_db != nullptr && canPerformQuickUnlock()) {
        // try to retrieve the stored password using Windows Hello
        QByteArray keyData;
        if (!getQuickUnlock()->getKey(m_db->publicUuid(), keyData)) {
            Q_EMIT showMessage(i18n("Failed to authenticate with Quick Unlock: %1").arg(getQuickUnlock()->errorString()), MessageType::Error);
            return {};
        }
        databaseKey->setRawKey(keyData);
        return databaseKey;
    }

    if (!m_password.isEmpty() || m_retryUnlockWithEmptyPassword) {
        databaseKey->addKey(QSharedPointer<PasswordKey>::create(m_password));
    }

    auto config = KSharedConfig::openConfig();
    auto lastKeyFiles = config->group(u"LastKeyFiles"_s);
    lastKeyFiles.deleteEntry(m_databaseUrl.toLocalFile());

    auto key = QSharedPointer<FileKey>::create();
    if (!m_keyFileUrl.isEmpty()) {
        QString errorMsg;
        if (!key->load(m_keyFileUrl.toLocalFile(), &errorMsg)) {
            Q_EMIT showMessage(i18n("Failed to open key file: %1").arg(errorMsg), MessageType::Error);
            return {};
        }
        if (key->type() != FileKey::KeePass2XMLv2 && key->type() != FileKey::Hashed) {
            Q_EMIT showMessage(i18n("You are using an old key file format which KDE KeyChain may stop supporting in the future"), MessageType::Warning);
        }
        databaseKey->addKey(key);
        if (KeychainConfig::self()->rememberLastKeyFiles()) {
            lastKeyFiles.writeEntry(m_databaseUrl.toLocalFile(), m_keyFileUrl.toLocalFile());
            config->sync();
        }
    }

    auto lastChallengeResponse = config->group(u"LastChallengeResponse"_s);
    lastChallengeResponse.deleteEntry(m_databaseUrl.toLocalFile());

    if (m_yubikeySlot) {
        auto crKey = QSharedPointer<ChallengeResponseKey>(new ChallengeResponseKey(*m_yubikeySlot));
        databaseKey->addChallengeResponseKey(crKey);

        // Qt doesn't read custom types in settings so stuff into a QString
        if (KeychainConfig::self()->rememberLastKeyFiles()) {
            lastChallengeResponse.writeEntry(m_databaseUrl.toLocalFile(), QStringLiteral("%1:%2").arg(m_yubikeySlot->first).arg(m_yubikeySlot->second));
            config->sync();
        }
    }

    return databaseKey;
}

bool DatabaseOpenHelper::userInteractionLock() const
{
    return m_unlockingDatabase;
}

void DatabaseOpenHelper::setUserInteractionLock(bool state)
{
    if (m_unlockingDatabase == state) {
        return;
    }
    m_unlockingDatabase = state;
    Q_EMIT userInteractionLockChanged();
}

bool DatabaseOpenHelper::canPerformQuickUnlock() const
{
    return m_db != nullptr && isQuickUnlockAvailable() && getQuickUnlock()->hasKey(m_db->publicUuid());
}

bool DatabaseOpenHelper::isOnQuickUnlockScreen() const
{
    return false; // m_ui->centralStack->currentIndex() == 1;
}

void DatabaseOpenHelper::toggleQuickUnlockScreen()
{
    // if (canPerformQuickUnlock()) {
    //     m_ui->centralStack->setCurrentIndex(1);
    //     // Work around qt issue where focus is stolen even if not visible
    //     if (m_ui->quickUnlockButton->isVisible()) {
    //         m_ui->quickUnlockButton->setFocus();
    //     }
    // } else {
    //     m_ui->centralStack->setCurrentIndex(0);
    //     // Work around qt issue where focus is stolen even if not visible
    //     if (m_ui->editPassword->isVisible()) {
    //         m_ui->editPassword->setFocus();
    //     }
    // }
}

void DatabaseOpenHelper::triggerQuickUnlock()
{
    if (isOnQuickUnlockScreen()) {
        // m_ui->quickUnlockButton->click();
    }
}

/**
 * Reset installed quick unlock secrets.
 *
 * It's safe to call this method even if quick unlock is unavailable.
 */
void DatabaseOpenHelper::resetQuickUnlock()
{
    if (!isQuickUnlockAvailable()) {
        return;
    }
    if (m_db != nullptr) {
        getQuickUnlock()->reset(m_db->publicUuid());
    }
}

YubiKeySlot DatabaseOpenHelper::yubikeySlot() const
{
    return *m_yubikeySlot;
}

void DatabaseOpenHelper::setYubikeySlot(YubiKeySlot _yubiKeySlot)
{
    if (m_yubikeySlot == _yubiKeySlot) {
        return;
    }

    m_yubikeySlot = _yubiKeySlot;
    Q_EMIT yubikeySlotChanged();
}

void DatabaseOpenHelper::resetYubikeySlot()
{
    m_yubikeySlot = std::nullopt;
    Q_EMIT yubikeySlotChanged();
}
