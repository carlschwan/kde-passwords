// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

import QtQuick
import QtQuick.Layouts
import QtQuick.Dialogs
import QtQuick.Controls as Controls
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.kirigamiaddons.components as Components
import org.kde.keychain

/// Unlock database page
FormCard.FormCardPage {
    id: root

    property alias databaseUrl: databaseOpenHelper.databaseUrl

    title: i18nc("@title:window", "Unlock Database")

    header: Components.Banner {
        id: banner

        showCloseButton: true
    }

    DatabaseOpenHelper {
        id: databaseOpenHelper

        onHasMinorVersionMismatch: (blockQuickUnlock, databaseKey) => {
            hasMinorVersionMismatchDialog.blockQuickUnlock = blockQuickUnlock;
            hasMinorVersionMismatchDialog.databaseKey = databaseKey;
            hasMinorVersionMismatchDialog.open();
        }

        onFinished: (database) => {
            Controller.currentDatabase = database;
            banner.visible = false;
        }

        onUnlockFailedNoPassword: () => {
            unlockFailedNoPassword.open()
        }

        onShowMessage: (message, type) => {
            banner.type = type;
            banner.text = message;
            banner.visible = true;
            passwordField.forceActiveFocus();
            passwordField.selectAll();
        }
    }

    Components.MessageDialog {
        id: unlockFailedNoPassword

        dialogType: Components.MessageDialog.Error
        title: i18n("Unlock failed and no password given")
        standardButtons: Controls.Dialog.Ok | Controls.Dialog.Cancel

        Controls.Label {
            text: i18n("Unlocking the database failed and you did not enter a password.\n Do you want to retry with an \"empty\" password instead?")
            wrapMode: Text.WordWrap

            Layout.fillWidth: true
        }

        onAccepted: {
            databaseOpenHelper.retryUnlockWithEmptyPassword();
            close();
        }
    }

    FileDialog {
        id: fileDialog

        title: i18nc("@title:window", "Select key file")
        fileMode: FileDialog.OpenFile
        onAccepted: databaseOpenHelper.keyFileUrl = selectedFile
        nameFilters: [i18n("Key files (*.keyx *.key)"), i18n("All files (*)")]
    }

    FormCard.FormHeader {
        title: root.title
    }

    FormCard.FormSectionText {
        text: databaseOpenHelper.databaseUrl.toString().substring(7)
    }

    FormCard.FormCard {
        FormCard.FormPasswordFieldDelegate {
            id: passwordField

            label: i18nc("@label:textbox", "Password:")
            onTextChanged: databaseOpenHelper.password = text
            onAccepted: databaseOpenHelper.openDatabase()
            enabled: !databaseOpenHelper.userInteractionLock
            focus: true
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormButtonDelegate {
            text: i18nc("@label:chooser", "Key File:")
            description: databaseOpenHelper.keyFileUrl.toString().length > 0 ? databaseOpenHelper.keyFileUrl.toString().substring(7) : i18nc("@info:placeholder", "No key file selected")
            onClicked: fileDialog.open()
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormComboBoxDelegate {
            id: hardwareKeyCombo

            text: i18nc("@label:listbox", "Hardware key:")
            model: YubikeySlotModel {
                onToggleHardwareKeyComponent: (state) => {
                    if (!state) {
                        hardwareKeyCombo.currentIndex = 0;
                    }
                }
            }
            textRole: "name"
            valueRole: "value"
            enabled: count > 1
            displayText: currentIndex === 0 ? i18nc("@info:placeholder", "Select hardware key...") : currentText
            currentIndex: 0
            onCurrentValueChanged: () =>{
                if (currentIndex === 0) {
                    databaseOpenHelper.yubikeySlot = undefined;
                } else {
                    databaseOpenHelper.yubikeySlot = currentValue;
                }
            }
        }
    }

    Kirigami.PromptDialog {
        id: hasMinorVersionMismatchDialog

        property bool blockQuickUnlock
        property var databaseKey

        dialogType: Kirigami.PromptDialog.Warning
        title: i18n("Database Version Mismatch")
        subtitle: i18n("The database you are trying to open was most likely created by a newer version of KDE KeyChain.\n\nYou can try to open it anyway, but it may be incomplete and saving any changes may incur data loss.\n\nWe recommend you update your KDE KeyChain installation.")

        onRejected: databaseOpenHelper.ignoreMinorVersionMismatch();
        onAccepted: databaseOpenHelper.saveQuickUnlockCredentialSlot(hasMinorVersionMismatchDialog.blockQuickUnlock, hasMinorVersionMismatchDialog.databaseKey);

        footer: Controls.DialogButtonBox {
            standardButtons: Controls.DialogButtonBox.Cancel

            Controls.Button {
                text: i18nc("@action:button", "Open database anyway")

                Controls.DialogButtonBox.buttonRole: Controls.DialogButtonBox.AcceptRole
            }
        }
    }
}
