// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include "core/Database.h"
#include <QObject>
#include <QtQml/qqmlregistration.h>

class Controller : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

    Q_PROPERTY(Database *currentDatabase READ currentDatabase WRITE setCurrentDatabase NOTIFY currentDatabaseChanged)
    Q_PROPERTY(Group *currentGroup READ currentGroup WRITE setCurrentGroup NOTIFY currentGroupChanged)

public:
    explicit Controller(QObject *parent = nullptr);
    ~Controller() override;

    Database *currentDatabase() const;
    void setCurrentDatabase(Database *database);

    Group *currentGroup() const;
    void setCurrentGroup(Group *group);

Q_SIGNALS:
    void currentDatabaseChanged();
    void currentGroupChanged();
    void showYubiKeyPopup();
    void hideYubiKeyPopup();

private:
    std::unique_ptr<Database> m_currentDatabase;
    Group *m_currentGroup = nullptr; // Group memory is managed by the Database
};
