// SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "iconimageprovider.h"

#include <QApplication>
#include <QBuffer>
#include <QDir>
#include <QDnsLookup>
#include <QFileInfo>
#include <QHostInfo>
#include <QImageReader>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkDiskCache>
#include <QNetworkReply>
#include <QStandardPaths>
#include <QThread>

#include <KLocalizedString>

static constexpr auto MAX_REDIRECTS = 5;

using namespace Qt::StringLiterals;

IconImageProvider::IconImageProvider()
    : QQuickAsyncImageProvider()
{
    qnam.setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);

    qnam.enableStrictTransportSecurityStore(true, QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + QLatin1StringView("/hsts/"));
    qnam.setStrictTransportSecurityEnabled(true);

    auto namDiskCache = new QNetworkDiskCache(&qnam);
    namDiskCache->setCacheDirectory(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + QLatin1StringView("/nam/"));
    qnam.setCache(namDiskCache);
}

QQuickImageResponse *IconImageProvider::requestImageResponse(const QString &domain, const QSize &requestedSize)
{
    return new ThumbnailResponse(domain, requestedSize, &qnam);
}

static QUrl convertVariantToUrl(const QVariant &var)
{
    QUrl url;
    if (var.canConvert<QUrl>()) {
        url = var.toUrl();
    }
    return url;
}

static QUrl getRedirectTarget(QNetworkReply *reply)
{
    const QVariant var = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    const QUrl url = convertVariantToUrl(var);
    return url;
}

static bool isIpAddress(const QString &host)
{
    // Handle IPv6 host with brackets, e.g [::1]
    const auto hostAddress = host.startsWith(u'[') && host.endsWith(u']') ? host.mid(1, host.length() - 2) : host;
    QHostAddress address(hostAddress);
    return address.protocol() == QAbstractSocket::IPv4Protocol || address.protocol() == QAbstractSocket::IPv6Protocol;
}

/**
 * Gets the top level domain from URL.
 *
 * Returns the TLD e.g. https://another.example.co.uk -> co.uk
 */
static QString getTopLevelDomainFromUrl(const QString &url)
{
    auto host = QUrl::fromUserInput(url).host();
    if (isIpAddress(host)) {
        return host;
    }

    const auto numberOfDomainParts = host.split(u'.').length();
    static const auto dummy = QByteArrayLiteral("");

    // Only loop the amount of different parts found
    for (auto i = 0; i < numberOfDomainParts; ++i) {
        // Cut the first part from host
        host = host.mid(host.indexOf(u'.') + 1);

        QNetworkCookie cookie(dummy, dummy);
        cookie.setDomain(host);

        // Check if dummy cookie's domain/TLD matches with public suffix list
        if (!QNetworkCookieJar{}.setCookiesFromUrl(QList{cookie}, QUrl::fromUserInput(url))) {
            return host;
        }
    }

    return host;
}

/**
 * Gets the base domain of URL or hostname.
 *
 * Returns the base domain, e.g. https://another.example.co.uk -> example.co.uk
 * Up-to-date list can be found: https://publicsuffix.org/list/public_suffix_list.dat
 */
static QString getBaseDomainFromUrl(const QString &url)
{
    auto qUrl = QUrl::fromUserInput(url);

    auto host = qUrl.host();
    if (isIpAddress(host)) {
        return host;
    }

    const auto tld = getTopLevelDomainFromUrl(qUrl.toString());
    if (tld.isEmpty() || tld.length() + 1 >= host.length()) {
        return host;
    }

    // Remove the top level domain part from the hostname, e.g. https://another.example.co.uk -> https://another.example
    host.chop(tld.length() + 1);
    // Split the URL and select the last part, e.g. https://another.example -> example
    QString baseDomain = host.split(u'.').last();
    // Append the top level domain back to the URL, e.g. example -> example.co.uk
    baseDomain.append(QStringLiteral(".%1").arg(tld));

    return baseDomain;
}

ThumbnailResponse::ThumbnailResponse(QString domain, QSize size, QNetworkAccessManager *qnam)
    : m_domain(std::move(domain))
    , requestedSize(size)
    , m_qnam(qnam)
    , m_redirects(0)
    , errorStr(i18n("Image request hasn't started"))
{
    m_domain = m_domain.trimmed().toLower();
    m_localFile = QStringLiteral("%1/favicon/%2.png").arg(QStandardPaths::writableLocation(QStandardPaths::CacheLocation), m_domain);

    QImage cachedImage;
    if (cachedImage.load(m_localFile)) {
        m_image = cachedImage;
        errorStr.clear();
        Q_EMIT finished();
        return;
    }

    QUrl url = QUrl::fromUserInput(m_domain);
    if (!url.isValid() || url.host().isEmpty()) {
        errorStr = u"Invalid url"_s;
        Q_EMIT finished();
        return;
    }

    m_urlsToTry.clear();

    // Fall back to https if no scheme is specified
    // fromUserInput defaults to http. Hence, we need to replace the default scheme should we detect that it has
    // been added by fromUserInput
    if (!m_domain.startsWith(url.scheme())) {
        url.setScheme(u"https"_s);
    } else if (url.scheme() != u"https"_s && url.scheme() != u"http"_s) {
        Q_EMIT finished();
        return;
    }

    // Remove query string - if any
    if (url.hasQuery()) {
        url.setQuery(QString());
    }
    // Remove fragment - if any
    if (url.hasFragment()) {
        url.setFragment(QString());
    }

    QString fullyQualifiedDomain = url.host();

    // Determine if host portion of URL is an IP address by resolving it and
    // searching for a match with the returned address(es).
    bool hostIsIp = false;
    QList<QHostAddress> hostAddressess = QHostInfo::fromName(fullyQualifiedDomain).addresses();
    hostIsIp = std::any_of(hostAddressess.begin(), hostAddressess.end(), [&fullyQualifiedDomain](const QHostAddress &addr) {
        return addr.toString() == fullyQualifiedDomain;
    });

    // Determine the second-level domain, if available
    QString secondLevelDomain;
    if (!hostIsIp) {
        secondLevelDomain = getBaseDomainFromUrl(url.toString());
    }

    // Start with the "fallback" url (if enabled) to try to get the best favicon
    QUrl fallbackUrl = QUrl(u"https://icons.duckduckgo.com"_s);
    fallbackUrl.setPath(u"/ip3/"_s + QString::fromUtf8(QUrl::toPercentEncoding(fullyQualifiedDomain)) + u".ico"_s);
    m_urlsToTry.append(fallbackUrl);

    // Also try a direct pull of the second-level domain (if possible)
    if (!hostIsIp && fullyQualifiedDomain != secondLevelDomain) {
        fallbackUrl.setPath(u"/ip3/"_s + QString::fromUtf8(QUrl::toPercentEncoding(secondLevelDomain)) + u".ico"_s);
        m_urlsToTry.append(fallbackUrl);
    }

    // Add a pull that preserves the query if there is one.
    if (!url.path().isEmpty()) {
        // Appends /favicon.ico to the last segment of the path.
        // stem/something/ will become stem/something/favicon.ico, and stem/something will become stem/favicon.ico
        m_urlsToTry.append(url.resolved(QUrl(u"./favicon.ico"_s)));
    }

    // Add a direct pull of the website's own favicon.ico file
    QUrl favicon_url = url;
    favicon_url.setPath(u"/favicon.ico"_s);
    m_urlsToTry.append(favicon_url);

    // Also try a direct pull of the second-level domain (if possible)
    if (!hostIsIp && fullyQualifiedDomain != secondLevelDomain && !secondLevelDomain.isEmpty()) {
        favicon_url.setHost(secondLevelDomain);
        m_urlsToTry.append(favicon_url);
        if (!favicon_url.userInfo().isEmpty() || favicon_url.port() != -1) {
            // Remove additional fields from the URL as a fallback. Keep only host and scheme
            // Fragment and query have been removed earlier
            favicon_url.setPort(-1);
            favicon_url.setUserInfo(QString());
            m_urlsToTry.append(favicon_url);
        }
    }

    // Execute a request on the main thread asynchronously
    moveToThread(QApplication::instance()->thread());
    QMetaObject::invokeMethod(this, &ThumbnailResponse::startRequest, Qt::QueuedConnection);
}

void ThumbnailResponse::startRequest()
{
    if (m_urlsToTry.isEmpty()) {
        Q_EMIT finished();
        errorStr = u"Unable to find favicon"_s;
        return;
    }

    auto fetchUrl = m_urlsToTry.takeFirst();
    qWarning() << "fetching" << fetchUrl;
    auto reply = m_qnam->get(QNetworkRequest(fetchUrl));
    connect(reply, &QNetworkReply::finished, this, [this, reply, fetchUrl]() {
        const bool error = (reply->error() != QNetworkReply::NoError);
        QUrl redirectTarget = getRedirectTarget(reply);

        if (!error) {
            if (redirectTarget.isValid()) {
                // Redirected, we need to follow it, or fall through if we have
                // done too many redirects already.
                if (m_redirects < MAX_REDIRECTS) {
                    m_redirects++;
                    if (redirectTarget.isRelative()) {
                        redirectTarget = fetchUrl.resolved(redirectTarget);
                    }
                    m_urlsToTry.prepend(redirectTarget);
                    startRequest();
                    return;
                } else {
                    if (reply->error() != QNetworkReply::NoError) {
                        Q_EMIT finished();
                        errorStr = u"Maximum redirection reached"_s;
                        return;
                    }
                }
            } else {
                // No redirect and no error
                imageQueried(reply);
                return;
            }
        } else {
            // Try the next url
            m_redirects = 0;
            startRequest();
        }
    });
}

/**
 * Parse fetched image bytes.
 *
 * Parses the given byte array into a QImage. Unlike QImage::loadFromData(), this method
 * tries to extract the highest resolution image from .ICO files.
 *
 * @param imageBytes raw image bytes
 * @return parsed image
 */
static QImage parseImage(QByteArray &imageBytes)
{
    QBuffer buff(&imageBytes);
    buff.open(QIODevice::ReadOnly);
    QImageReader reader(&buff);

    if (reader.imageCount() <= 0) {
        return reader.read();
    }

    QImage img;
    for (int i = 0; i < reader.imageCount(); ++i) {
        if (img.isNull() || reader.size().width() > img.size().width()) {
            img = reader.read();
        }
        reader.jumpToNextImage();
    }

    return img;
}

void ThumbnailResponse::imageQueried(QNetworkReply *reply)
{
    QWriteLocker _(&lock);
    reply->deleteLater();

    QByteArray imageData = reply->readAll();
    m_image = parseImage(imageData);
    if (!m_image.isNull()) {
        QString localPath = QFileInfo(m_localFile).absolutePath();
        QDir dir;
        if (!dir.exists(localPath)) {
            dir.mkpath(localPath);
        }

        m_image.save(m_localFile);
        errorStr.clear();
    } else {
        errorStr = u"Invalid image"_s;
    }
    Q_EMIT finished();
}

void ThumbnailResponse::doCancel()
{
}

QQuickTextureFactory *ThumbnailResponse::textureFactory() const
{
    QReadLocker _(&lock);
    return QQuickTextureFactory::textureFactoryForImage(m_image);
}

QString ThumbnailResponse::errorString() const
{
    QReadLocker _(&lock);
    return errorStr;
}

void ThumbnailResponse::cancel()
{
    QMetaObject::invokeMethod(this, &ThumbnailResponse::doCancel, Qt::QueuedConnection);
}

#include "moc_iconimageprovider.cpp"
