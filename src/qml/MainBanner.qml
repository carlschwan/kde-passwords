// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import org.kde.kirigamiaddons.components as Components
import org.kde.keychain

Components.Banner {
    id: root

    Connections {
        target: Controller

        function onShowYubiKeyPopup(): void {
            root.text = i18nc("@info", "Please present or touch your YubiKey to continue…");
            root.visible = true;
        }

        function onHideYubiKeyPopup(): void {
            root.visible = false;
        }
    }
}
