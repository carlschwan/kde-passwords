// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.keychain

FormCard.FormCardPage {
    id: root

    property Entry entry

    title: root.entry?.title

    actions: Controls.Action {
        text: i18nc("@action:inmenu", "Edit")
        icon.name: 'document-edit-symbolic'
        onTriggered: root.Controls.ApplicationWindow.window.pageStack.pushDialogLayer(
            Qt.createComponent('org.kde.keychain', 'EntryEditorPage'),
            {
                entry: contextMenu.entry,
                mode: EntryEditorPage.Edit,
            }
        );
    }

    FormCard.FormCard {
        Layout.topMargin: Kirigami.Units.largeSpacing * 4

        FormCard.FormTextDelegate {
            text: i18nc("@label", "Url:")
            description: root.entry?.url
            visible: root.entry?.url.length > 0
        }

        FormCard.FormDelegateSeparator {
        }

        FormCard.FormTextDelegate {
            id: usernameDelegate
            text: i18nc("@label", "Username:")
            description: root.entry.username
            visible: root.entry.username.length > 0
        }

        FormCard.FormDelegateSeparator {
            visible: root.entry.password.length > 0
        }

        FormCard.FormTextDelegate {
            visible: root.entry.password.length > 0
            text: i18nc("@label", "Password:")
            description: root.entry.password
        }

        FormCard.FormDelegateSeparator {
            visible: root.entry.notes.length > 0
        }

        FormCard.FormTextDelegate {
            visible: root.entry.notes.length > 0
            text: i18nc("@label", "Notes:")
            description: root.entry.notes
            descriptionItem.wrapMode: Text.Wrap
        }
    }
}
