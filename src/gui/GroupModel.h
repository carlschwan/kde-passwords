// SPDX-FileCopyrightText: 2010 Felix Geyer <debfx@fobos.de>
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only

#pragma once

#include <QAbstractItemModel>
#include <QtQml/qqmlregistration.h>

class Database;
class Group;

class GroupModel : public QAbstractItemModel
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(Database *database READ database WRITE setDatabase NOTIFY databaseChanged)

public:
    enum ExtraRole {
        GroupNameRole = Qt::UserRole + 1,
        IconNameRole,
        IsExpiredRole,
        GroupRole,
    };

    explicit GroupModel(QObject *parent = nullptr);
    Database *database() const;
    void setDatabase(Database *newDb);
    [[nodiscard]] QModelIndex index(Group *group) const;
    Group *groupFromIndex(const QModelIndex &index) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Qt::DropActions supportedDropActions() const override;
    [[nodiscard]] Qt::ItemFlags flags(const QModelIndex &modelIndex) const override;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;
    [[nodiscard]] QStringList mimeTypes() const override;
    QMimeData *mimeData(const QModelIndexList &indexes) const override;
    void sortChildren(Group *rootGroup, bool reverse = false);

private:
    QModelIndex parent(Group *group) const;
    void collectIndexesRecursively(QList<QModelIndex> &indexes, const QList<Group *> &groups);

    void groupDataChanged(Group *group);
    void groupAboutToRemove(Group *group);
    void groupRemoved();
    void groupAboutToAdd(Group *group, int index);
    void groupAdded();
    void groupAboutToMove(Group *group, Group *toGroup, int pos);
    void groupMoved();

Q_SIGNALS:
    void databaseChanged();

private:
    Database *m_db = nullptr;
};
