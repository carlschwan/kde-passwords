/*
 *  Copyright (C) 2019 KeePassXC Team <team@keepassxc.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 or (at your option)
 *  version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "OpVaultReader.h"

#include "core/Entry.h"
#include "core/Totp.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QUrlQuery>

namespace
{
    QDateTime resolveDate(const QString& kind, const QJsonValue& value)
    {
        QDateTime date;
        if (kind == QLatin1StringView("monthYear")) {
            // 1Password programmers are sadistic...
            auto dateValue = QString::number(value.toInt());
            date = QDateTime::fromString(dateValue, QStringLiteral("yyyyMM"));
            date.setTimeSpec(Qt::UTC);
        } else if (value.isString()) {
            date = QDateTime::fromSecsSinceEpoch(value.toString().toUInt(), Qt::UTC);
        } else {
            date = QDateTime::fromSecsSinceEpoch(value.toInt(), Qt::UTC);
        }
        return date;
    }
} // namespace

void OpVaultReader::fillFromSection(Entry* entry, const QJsonObject& section)
{
    const auto uuid = entry->uuid();
    auto sectionTitle = section[QLatin1StringView("title")].toString();

    if (!section.contains(QLatin1StringView("fields"))) {
        auto sectionName = section[QLatin1StringView("name")].toString();
        if (!(sectionName.toLower() == QLatin1StringView("linked items") && sectionTitle.toLower() == QLatin1StringView("related items"))) {
            qWarning() << R"(Skipping "fields"-less Section in UUID ")" << uuid << "\": <<" << section << ">>";
        }
        return;
    } else if (!section[QLatin1StringView("fields")].isArray()) {
        qWarning() << R"(Skipping non-Array "fields" in UUID ")" << uuid << "\"\n";
        return;
    }

    const QJsonArray sectionFields = section[QLatin1StringView("fields")].toArray();
    for (const QJsonValue &sectionField : sectionFields) {
        if (!sectionField.isObject()) {
            qWarning() << R"(Skipping non-Object "fields" in UUID ")" << uuid << "\": << " << sectionField << ">>";
            continue;
        }
        fillFromSectionField(entry, sectionTitle, sectionField.toObject());
    }
}

void OpVaultReader::fillFromSectionField(Entry* entry, const QString& sectionName, const QJsonObject& field)
{
    if (!field.contains(QLatin1StringView("v"))) {
        // for our purposes, we don't care if there isn't a value in the field
        return;
    }

    // Ignore "a" and "inputTraits" fields, they don't apply to KPXC

    auto attrName = resolveAttributeName(sectionName, field[QLatin1StringView("n")].toString(), field[QLatin1StringView("t")].toString());
    auto attrValue = field.value(QLatin1StringView("v")).toString();
    auto kind = field[QLatin1StringView("k")].toString();

    if (attrName.startsWith(QStringLiteral("TOTP_"))) {
        if (entry->hasTotp()) {
            // Store multiple TOTP definitions as additional otp attributes
            int i = 0;
            QString name(QStringLiteral("otp"));
            auto attributes = entry->attributes()->keys();
            while (attributes.contains(name)) {
                name = QStringLiteral("otp_%1").arg(++i);
            }
            entry->attributes()->set(name, attrValue, true);
        } else if (attrValue.startsWith(QLatin1StringView("otpauth://"))) {
            QUrlQuery query(attrValue);
            // at least as of 1Password 7, they don't append the digits= and period= which totp.cpp requires
            if (!query.hasQueryItem(QStringLiteral("digits"))) {
                query.addQueryItem(QStringLiteral("digits"), QStringLiteral("%1").arg(Totp::DEFAULT_DIGITS));
            }
            if (!query.hasQueryItem(QStringLiteral("period"))) {
                query.addQueryItem(QStringLiteral("period"), QStringLiteral("%1").arg(Totp::DEFAULT_STEP));
            }
            attrValue = query.toString(QUrl::FullyEncoded);
            entry->setTotp(Totp::parseSettings(attrValue));
        } else {
            entry->setTotp(Totp::parseSettings({}, attrValue));
        }

    } else if (attrName.startsWith(QLatin1StringView("expir"), Qt::CaseInsensitive)) {
        QDateTime expiry = resolveDate(kind, field.value(QLatin1StringView("v")));
        if (expiry.isValid()) {
            entry->setExpiryTime(expiry);
            entry->setExpires(true);
        } else {
            qWarning() << QStringLiteral("[%1] Invalid expiration date found: %2").arg(entry->title(), attrValue);
        }
    } else {
        if (kind == QLatin1StringView("date") || kind == QLatin1StringView("monthYear")) {
            QDateTime date = resolveDate(kind, field.value(QLatin1StringView("v")));
            if (date.isValid()) {
                entry->attributes()->set(attrName, QLocale().toString(date, QLocale::ShortFormat));
            } else {
                qWarning()
                    << QStringLiteral("[%1] Invalid date attribute found: %2 = %3").arg(entry->title(), attrName, attrValue);
            }
        } else if (kind == QLatin1StringView("address")) {
            // Expand address into multiple attributes
            auto addrFields = field.value(QLatin1StringView("v")).toObject().toVariantMap();
            const auto parts = addrFields.keys();
            for (const auto& part : parts) {
                entry->attributes()->set(attrName + QStringLiteral("_%1").arg(part), addrFields.value(part).toString());
            }
        } else {
            if (entry->attributes()->hasKey(attrName)) {
                // Append a random string to the attribute name to avoid collisions
                attrName += QStringLiteral("_%1").arg(QUuid::createUuid().toString().mid(1, 5));
            }
            entry->attributes()->set(attrName, attrValue, (kind == QLatin1StringView("password") || kind == QLatin1StringView("concealed")));
        }
    }
}

QString OpVaultReader::resolveAttributeName(const QString& section, const QString& name, const QString& text)
{
    // Special case for TOTP
    if (name.startsWith(QLatin1StringView("TOTP_"))) {
        return name;
    }

    auto lowName = name.toLower();
    auto lowText = text.toLower();
    if (section.isEmpty() || name.startsWith(QLatin1StringView("address"))) {
        // Empty section implies these are core attributes
        // try to find username, password, url
        if (lowName == QLatin1StringView("password") || lowText == QLatin1StringView("password")) {
            return EntryAttributes::PasswordKey;
        } else if (lowName == QLatin1StringView("username") || lowText == QLatin1StringView("username")) {
            return EntryAttributes::UserNameKey;
        } else if (lowName == QLatin1StringView("url") || lowText == QLatin1StringView("url") || lowName == QLatin1StringView("hostname") || lowText == QLatin1StringView("server")
                   || lowName == QLatin1StringView("website")) {
            return EntryAttributes::URLKey;
        }
        return text;
    }

    return QStringLiteral("%1_%2").arg(section, text);
}
