// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "yubikeyslotmodel.h"

#include "keychainconfig.h"
#include "keys/drivers/YubiKeyInterfaceUSB.h"

#include <KConfigGroup>
#include <KLocalizedString>
#include <KSharedConfig>

#include <QTimer>

using namespace Qt::StringLiterals;

YubikeySlotModel::YubikeySlotModel(QObject *parent)
    : QAbstractListModel(parent)
    , m_deviceListener(new DeviceListener(this))
{
    connect(m_deviceListener, &DeviceListener::devicePlugged, this, [this](bool state, void *ctx, void *device) {
        Q_UNUSED(state);
        Q_UNUSED(ctx);
        Q_UNUSED(device);

        pollHardwareKey();
    });

    connect(YubiKey::instance(), &YubiKey::detectComplete, this, &YubikeySlotModel::hardwareKeyResponse, Qt::QueuedConnection);

#ifdef Q_OS_WIN
    m_deviceListener->registerHotplugCallback(true, true, YubiKeyInterfaceUSB::YUBICO_USB_VID, DeviceListener::MATCH_ANY, &DeviceListenerWin::DEV_CLS_KEYBOARD);
    m_deviceListener->registerHotplugCallback(true,
                                              true,
                                              YubiKeyInterfaceUSB::ONLYKEY_USB_VID,
                                              DeviceListener::MATCH_ANY,
                                              &DeviceListenerWin::DEV_CLS_KEYBOARD);
#else
    m_deviceListener->registerHotplugCallback(true, true, YubiKeyInterfaceUSB::YUBICO_USB_VID);
    m_deviceListener->registerHotplugCallback(true, true, YubiKeyInterfaceUSB::ONLYKEY_USB_VID);
#endif

    pollHardwareKey();
}

YubikeySlotModel::~YubikeySlotModel()
{
    m_deviceListener->deregisterAllHotplugCallbacks();
}

bool YubikeySlotModel::pollingHardwareKey() const
{
    return m_pollingHardwareKey;
}

void YubikeySlotModel::setPollingHardwareKey(bool pollingHardwareKey)
{
    if (m_pollingHardwareKey == pollingHardwareKey) {
        return;
    }

    m_pollingHardwareKey = pollingHardwareKey;
    Q_EMIT pollingHardwareKeyChanged();
}

void YubikeySlotModel::pollHardwareKey(bool manualTrigger)
{
    if (m_pollingHardwareKey) {
        return;
    }

    setPollingHardwareKey(true);
    m_manualHardwareKeyRefresh = manualTrigger;

    // Add a delay, if this is an automatic trigger, to allow the USB device to settle as
    // the device may not report a valid serial number immediately after plugging in
    int delay = manualTrigger ? 0 : 500;
    QTimer::singleShot(delay, this, [] {
        YubiKey::instance()->findValidKeysAsync();
    });
}

int YubikeySlotModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    const auto count = YubiKey::instance()->foundKeys().count();
    return count > 0 ? count + 1 : 0;
}

QVariant YubikeySlotModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(checkIndex(index, QAbstractItemModel::CheckIndexOption::IndexIsValid));

    if (index.row() == 0) {
        switch (role) {
        case Qt::DisplayRole:
            return i18nc("@info:placeholder No hardware token selected", "None");
        default:
            return {};
        }
    }

    const auto foundKeys = YubiKey::instance()->foundKeys();
    const auto mapKeys = foundKeys.keys();
    const auto yubikeySlot = mapKeys[index.row() - 1];

    switch (role) {
    case Qt::DisplayRole:
        return foundKeys[yubikeySlot];
    case ValueRole:
        return QVariant::fromValue(yubikeySlot);
    default:
        return {};
    }
}

QHash<int, QByteArray> YubikeySlotModel::roleNames() const
{
    return {
        {Qt::DisplayRole, "name"},
        {ValueRole, "value"},
    };
}

void YubikeySlotModel::hardwareKeyResponse(bool found)
{
    setPollingHardwareKey(false);
    beginResetModel();
    endResetModel();

    if (!found) {
        Q_EMIT toggleHardwareKeyComponent(false);
        return;
    }

    YubiKeySlot lastUsedSlot;

    if (KeychainConfig::self()->rememberLastKeyFiles()) {
        auto config = KSharedConfig::openConfig();
        auto lastChallengeResponse = config->group(u"LastChallengeResponse"_s);
        if (lastChallengeResponse.hasKey(m_filename)) {
            // Qt doesn't read custom types in settings so extract from QString
            const auto split = lastChallengeResponse.readEntry(m_filename).split(u':');
            if (split.size() > 1) {
                lastUsedSlot = YubiKeySlot(split[0].toUInt(), split[1].toInt());
            }
            m_useHardwareKey = true;
        }
    }

    int idx = 0;
    const auto foundKeys = YubiKey::instance()->foundKeys();
    for (auto i = foundKeys.cbegin(); i != foundKeys.cend(); ++i) {
        // add detected YubiKey to combo box
        // Select this YubiKey + Slot if we used it in the past
        if (lastUsedSlot == i.key()) {
            Q_EMIT lastSelectedIndexFound(idx);
            break;
        }
        idx++;
    }

    Q_EMIT toggleHardwareKeyComponent(true);
}
