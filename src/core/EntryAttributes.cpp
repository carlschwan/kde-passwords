/*
 *  Copyright (C) 2023 KeePassXC Team <team@keepassxc.org>
 *  Copyright (C) 2012 Felix Geyer <debfx@fobos.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 or (at your option)
 *  version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "EntryAttributes.h"
#include "core/Global.h"

#include <QRegularExpression>
#include <QUuid>

const QString EntryAttributes::TitleKey = QStringLiteral("Title");
const QString EntryAttributes::UserNameKey = QStringLiteral("UserName");
const QString EntryAttributes::PasswordKey = QStringLiteral("Password");
const QString EntryAttributes::URLKey = QStringLiteral("URL");
const QString EntryAttributes::NotesKey = QStringLiteral("Notes");
const QStringList EntryAttributes::DefaultAttributes(QStringList()
                                                     << TitleKey << UserNameKey << PasswordKey << URLKey << NotesKey);
const QString EntryAttributes::WantedFieldGroupName = QStringLiteral("WantedField");
const QString EntryAttributes::SearchInGroupName = QStringLiteral("SearchIn");
const QString EntryAttributes::SearchTextGroupName = QStringLiteral("SearchText");

const QString EntryAttributes::RememberCmdExecAttr = QStringLiteral("_EXEC_CMD");
const QString EntryAttributes::AdditionalUrlAttribute = QStringLiteral("KP2A_URL");
const QString EntryAttributes::PasskeyAttribute = QStringLiteral("KPEX_PASSKEY");

EntryAttributes::EntryAttributes(QObject* parent)
    : ModifiableObject(parent)
{
    clear();
}

QList<QString> EntryAttributes::keys() const
{
    return m_attributes.keys();
}

bool EntryAttributes::hasKey(const QString& key) const
{
    return m_attributes.contains(key);
}

bool EntryAttributes::hasPasskey() const
{
    const auto keyList = keys();
    for (const auto& key : keyList) {
        if (isPasskeyAttribute(key)) {
            return true;
        }
    }

    return false;
}

QList<QString> EntryAttributes::customKeys() const
{
    QList<QString> customKeys;
    const QList<QString> keyList = keys();
    for (const QString& key : keyList) {
        if (!isDefaultAttribute(key) && !isPasskeyAttribute(key)) {
            customKeys.append(key);
        }
    }
    return customKeys;
}

QString EntryAttributes::value(const QString& key) const
{
    return m_attributes.value(key);
}

QList<QString> EntryAttributes::values(const QList<QString>& keys) const
{
    QList<QString> values;
    for (const QString& key : keys) {
        values.append(m_attributes.value(key));
    }
    return values;
}

bool EntryAttributes::contains(const QString& key) const
{
    return m_attributes.contains(key);
}

bool EntryAttributes::containsValue(const QString& value) const
{
    return asConst(m_attributes).values().contains(value);
}

bool EntryAttributes::isProtected(const QString& key) const
{
    return m_protectedAttributes.contains(key);
}

bool EntryAttributes::isReference(const QString& key) const
{
    if (!m_attributes.contains(key)) {
        Q_ASSERT(false);
        return false;
    }

    const QString data = value(key);
    return matchReference(data).hasMatch();
}

void EntryAttributes::set(const QString& key, const QString& value, bool protect)
{
    bool shouldEmitModified = false;

    bool addAttribute = !m_attributes.contains(key);
    bool changeValue = !addAttribute && (m_attributes.value(key) != value);
    bool defaultAttribute = isDefaultAttribute(key);

    if (addAttribute && !defaultAttribute) {
        Q_EMIT aboutToBeAdded(key);
    }

    if (addAttribute || changeValue) {
        m_attributes.insert(key, value);
        shouldEmitModified = true;
    }

    if (protect) {
        if (!m_protectedAttributes.contains(key)) {
            shouldEmitModified = true;
        }
        m_protectedAttributes.insert(key);
    } else if (m_protectedAttributes.remove(key)) {
        shouldEmitModified = true;
    }

    if (shouldEmitModified) {
        emitModified();
    }

    if (defaultAttribute && changeValue) {
        Q_EMIT defaultKeyModified();
    } else if (addAttribute) {
        Q_EMIT added(key);
    } else if (shouldEmitModified) {
        Q_EMIT customKeyModified(key);
    }
}

void EntryAttributes::remove(const QString& key)
{
    Q_ASSERT(!isDefaultAttribute(key));

    if (!m_attributes.contains(key)) {
        return;
    }

    Q_EMIT aboutToBeRemoved(key);

    m_attributes.remove(key);
    m_protectedAttributes.remove(key);

    Q_EMIT removed(key);
    emitModified();
}

void EntryAttributes::rename(const QString& oldKey, const QString& newKey)
{
    Q_ASSERT(!isDefaultAttribute(oldKey));
    Q_ASSERT(!isDefaultAttribute(newKey));

    if (!m_attributes.contains(oldKey)) {
        Q_ASSERT(false);
        return;
    }

    if (m_attributes.contains(newKey)) {
        Q_ASSERT(false);
        return;
    }

    QString data = value(oldKey);
    bool protect = isProtected(oldKey);

    Q_EMIT aboutToRename(oldKey, newKey);

    m_attributes.remove(oldKey);
    m_attributes.insert(newKey, data);
    if (protect) {
        m_protectedAttributes.remove(oldKey);
        m_protectedAttributes.insert(newKey);
    }

    emitModified();
    Q_EMIT renamed(oldKey, newKey);
}

void EntryAttributes::copyCustomKeysFrom(const EntryAttributes* other)
{
    if (!areCustomKeysDifferent(other)) {
        return;
    }

    Q_EMIT aboutToBeReset();

    // remove all non-default keys
    const QList<QString> keyList = keys();
    for (const QString& key : keyList) {
        if (!isDefaultAttribute(key)) {
            m_attributes.remove(key);
            m_protectedAttributes.remove(key);
        }
    }

    const QList<QString> otherKeyList = other->keys();
    for (const QString& key : otherKeyList) {
        if (!isDefaultAttribute(key)) {
            m_attributes.insert(key, other->value(key));
            if (other->isProtected(key)) {
                m_protectedAttributes.insert(key);
            }
        }
    }

    Q_EMIT reset();
    emitModified();
}

bool EntryAttributes::areCustomKeysDifferent(const EntryAttributes* other)
{
    const auto keysList = keys();
    const auto otherKeysList = other->keys();
    const QSet<QString> keysSet{keysList.cbegin(), keysList.cend()};
    const QSet<QString> otherKeysSet{otherKeysList.cbegin(), otherKeysList.cend()};

    // check if they are equal ignoring the order of the keys
    if (keysSet != otherKeysSet) {
        return true;
    }

    for (const QString& key : keysList) {
        if (isDefaultAttribute(key)) {
            continue;
        }

        if (isProtected(key) != other->isProtected(key) || value(key) != other->value(key)) {
            return true;
        }
    }

    return false;
}

void EntryAttributes::copyDataFrom(const EntryAttributes* other)
{
    if (*this != *other) {
        Q_EMIT aboutToBeReset();

        m_attributes = other->m_attributes;
        m_protectedAttributes = other->m_protectedAttributes;

        Q_EMIT reset();
        emitModified();
    }
}

QUuid EntryAttributes::referenceUuid(const QString& key) const
{
    if (!m_attributes.contains(key)) {
        Q_ASSERT(false);
        return {};
    }

    auto match = matchReference(value(key));
    if (match.hasMatch()) {
        const QString uuid = match.captured(QStringLiteral("SearchText"));
        if (!uuid.isEmpty()) {
            return QUuid::fromRfc4122(QByteArray::fromHex(uuid.toLatin1()));
        }
    }

    return {};
}

bool EntryAttributes::operator==(const EntryAttributes& other) const
{
    return (m_attributes == other.m_attributes && m_protectedAttributes == other.m_protectedAttributes);
}

bool EntryAttributes::operator!=(const EntryAttributes& other) const
{
    return (m_attributes != other.m_attributes || m_protectedAttributes != other.m_protectedAttributes);
}

QRegularExpressionMatch EntryAttributes::matchReference(const QString& text)
{
    static QRegularExpression referenceRegExp(QStringLiteral(
        "\\{REF:(?<WantedField>[TUPANI])@(?<SearchIn>[TUPANIO]):(?<SearchText>[^}]+)\\}"),
        QRegularExpression::CaseInsensitiveOption);

    return referenceRegExp.match(text);
}

void EntryAttributes::clear()
{
    Q_EMIT aboutToBeReset();

    m_attributes.clear();
    m_protectedAttributes.clear();

    for (const QString& key : DefaultAttributes) {
        m_attributes.insert(key, {});
    }

    Q_EMIT reset();
    emitModified();
}

int EntryAttributes::attributesSize() const
{
    int size = 0;
    for (auto it = m_attributes.constBegin(); it != m_attributes.constEnd(); ++it) {
        size += it.key().toUtf8().size() + it.value().toUtf8().size();
    }
    return size;
}

bool EntryAttributes::isDefaultAttribute(const QString& key)
{
    return DefaultAttributes.contains(key);
}

bool EntryAttributes::isPasskeyAttribute(const QString& key)
{
    return key.startsWith(PasskeyAttribute);
}
