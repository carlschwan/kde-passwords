// SPDX-License-Identifier: LGPL-2.0-or-later
// SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>

import QtQuick
import QtQuick.Controls as Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.formcard as FormCard
import org.kde.keychain

FormCard.FormCardPage {
    id: root

    signal accepted

    enum Mode {
        Edit,
        Create
    }

    property Entry entry
    property EntryEditorPage.Mode mode

    title: mode === EntryEditorPage.Create ? i18nc("@title", "Add New Entry") : root.entry?.title

    header: MainBanner {}

    FormCard.FormCard {
        Layout.topMargin: Kirigami.Units.largeSpacing * 4

        FormCard.FormTextFieldDelegate {
            id: titleField

            label: i18nc("@label:textbox", "Title:")
            text: root.entry?.title
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormTextFieldDelegate {
            id: urlField

            label: i18nc("@label:textbox", "Url:")
            text: root.entry?.url
            onTextChanged: if (titleField.text.length === 0) {
                titleField.text = text.replace(/.*:\/\//, '')
            }
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormTextFieldDelegate {
            id: usernameField

            label: i18nc("@label:textbox", "Username:")
            text: root.entry?.username
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormPasswordFieldDelegate {
            id: passwordField

            label: i18nc("@label:textbox", "Password:")
            text: root.entry.password
        }

        FormCard.FormDelegateSeparator {}

        FormCard.FormTextAreaDelegate {
            id: notesArea

            label: i18nc("@label", "Notes:")
            text: root.entry.notes
        }
    }

    footer: Controls.ToolBar {
        contentItem: Controls.DialogButtonBox {
            standardButtons: Controls.Dialog.Save | Controls.Dialog.Cancel

            onRejected: root.closeDialog()
            onAccepted: {
                if (root.mode === EntryEditorPage.Edit) {
                    root.entry.beginUpdate();
                }

                root.entry.title = titleField.text;
                root.entry.url = urlField.text;
                root.entry.username = usernameField.text;
                root.entry.password = passwordField.text;
                root.entry.notes = notesArea.text;

                if (root.mode === EntryEditorPage.Edit) {
                    root.entry.endUpdate();
                }

                root.accepted();

                if (Controller.currentDatabase.save()) {
                    root.closeDialog();
                } else {
                    console.error("Unable to save database");
                }
            }
        }
    }
}
